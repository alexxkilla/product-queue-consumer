package com.skytouchtask.examplequeue.service;

import com.skytouchtask.examplequeue.Product;
import com.skytouchtask.examplequeue.RabbitConfig;
import com.skytouchtask.examplequeue.repos.ProductsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductReceiver {

    static final Logger logger = LoggerFactory.getLogger(ProductReceiver.class);

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductReceiver(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @RabbitListener(queues = RabbitConfig.PRODUCT_QUEUE)
    public void processProduct(final Product product) {
        logger.info("Product Received: " + product.toString());
        Product productSaved = productsRepository.save(product);
        logger.info("Product should be saved to the db + " + productSaved.toString());
    }
}
