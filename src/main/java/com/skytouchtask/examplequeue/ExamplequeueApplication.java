package com.skytouchtask.examplequeue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages="com.skytouchtask.examplequeue.repos")
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class ExamplequeueApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamplequeueApplication.class, args);
    }

}
