package com.skytouchtask.examplequeue.repos;

import com.skytouchtask.examplequeue.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsRepository extends JpaRepository<Product, Long> {
}
