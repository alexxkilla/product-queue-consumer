package com.skytouchtask.examplequeue;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    public static final String PRODUCT_QUEUE = "product-queue";
    public static final String PRODUCT_LIST_QUEUE = "product-list-queue";
    public static final String PRODUCT_EXCHANGE = "product-exchange";

    @Bean
    Queue productQueue(){
        return QueueBuilder.durable(PRODUCT_QUEUE).build();
    }

//    @Bean
//    Queue productListQueue(){
//        return QueueBuilder.durable(PRODUCT_LIST_QUEUE).build();
//    }



    @Bean
    Exchange productsExchange() {
        return ExchangeBuilder.topicExchange(PRODUCT_EXCHANGE).build();
    }

    @Bean
    Binding binding(Queue productsQueue, TopicExchange productsExchange) {
        return BindingBuilder.bind(productsQueue).to(productsExchange).with(PRODUCT_QUEUE);
    }

    @Bean
    public Jackson2JsonMessageConverter producerMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerMessageConverter());
        return rabbitTemplate;
    }
}
